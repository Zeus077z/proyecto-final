import random as r


# Clase nave, de la cual van a ir saliendo los objetos
class Naves:
    def __init__(self, identificador, valor, municiones):
        self.identificador = identificador
        self.valor = valor
        self.municiones = municiones


# Lista que guardará las cuatro naves extraterrestres
l_vehiculos = list()
nave_jouk = Naves("N.Jouk", 1, r.randint(1, 4))
nave_tuak = Naves("N.Tuak", 1, r.randint(1, 4))
nave_rex = Naves("N.Rex", 1, r.randint(1, 4))
nave_hunt = Naves("N.Hunt", 1, r.randint(1, 4))
if not nave_rex in l_vehiculos:
    l_vehiculos.append(nave_rex)
    if not nave_tuak in l_vehiculos:
        l_vehiculos.append(nave_tuak)
        if not nave_jouk in l_vehiculos:
            l_vehiculos.append(nave_jouk)
            if not nave_hunt in l_vehiculos:
                l_vehiculos.append(nave_hunt)
                if not 0 in l_vehiculos:
                    l_vehiculos.append(0)

# Lista que guardará los cuatro vehiculos terrestres
l_vehiculos2 = list()
tanque = Naves("Tanque", 1, r.randint(1, 4))
vortex = Naves("Vortex", 1, r.randint(1, 4))
helicop = Naves("Helicoptero", 1, r.randint(1, 4))
barco = Naves("Barco", 1, r.randint(1, 4))
if not tanque in l_vehiculos2:
    l_vehiculos2.append(tanque)
    if not vortex in l_vehiculos2:
        l_vehiculos2.append(vortex)
        if not helicop in l_vehiculos2:
            l_vehiculos2.append(helicop)
            if not barco in l_vehiculos2:
                l_vehiculos2.append(barco)
                if not 0 in l_vehiculos2:
                    l_vehiculos2.append(0)
# Para la creacion de la primera matriz
n = 4
m = 4
lista = []

for i in range(n):
    lista.append([r.choice(l_vehiculos) for i in range(m)])

# Para la creacion de la segunda matriz
n1 = 4
m1 = 4
lista2 = []
for k in range(n1):
    lista2.append([r.choice(l_vehiculos2) for i in range(m1)])

# Para los turnos
turno = True

# Variables que guardan el total de balas
total_bal = 0
total_bal2 = 0

# Variables que guardan la cantidad de naves en total
total_nav = 0
total_nav2 = 0

# Variables de guardan la cantidad de naves de cada tipo
naves_rex = 0
naves_tuak = 0
naves_jouk = 0
naves_hunt = 0

# Variables que guardan la cantidad de vehiculos de cada tipo
tanques = 0
helicopteros = 0
barcos = 0
vortexs = 0

# Variables que guardarán la cantidad total de naves derrivadas de cada jugador
naves_derr = 0
naves_derr2 = 0
# Para restar municiones y naves


# Todos los ciclos que se encargarán de buscar cuantas naves hay que cada una y tambien busque cuantas balas tiene
for i in range(0, 4):
    for j in range(0, 4):
        if lista[i][j] == nave_rex:
            total_bal += nave_rex.municiones
            total_nav += 1
            naves_rex += 1

for i in range(0, 4):
    for j in range(0, 4):
        if lista[i][j] == nave_tuak:
            total_bal += nave_tuak.municiones
            total_nav += 1
            naves_tuak += 1
for i in range(0, 4):
    for j in range(0, 4):
        if lista[i][j] == nave_jouk:
            total_bal += nave_jouk.municiones
            total_nav += 1
            naves_jouk += 1
for i in range(0, 4):
    for j in range(0, 4):
        if lista[i][j] == nave_hunt:
            total_bal += nave_hunt.municiones
            total_nav += 1
            naves_hunt += 1

# Lo mismo que el anterior, pero con los vehículosde la tierra
for i in range(0, 4):
    for j in range(0, 4):
        if lista2[i][j] == tanque:
            total_bal2 += tanque.municiones
            total_nav2 += 1
            tanques += 1
for i in range(0, 4):
    for j in range(0, 4):
        if lista2[i][j] == vortex:
            total_bal2 += vortex.municiones
            total_nav2 += 1
            vortexs += 1
for i in range(0, 4):
    for j in range(0, 4):
        if lista2[i][j] == helicop:
            total_bal2 += helicop.municiones
            total_nav2 += 1
            helicopteros += 1
for i in range(0, 4):
    for j in range(0, 4):
        if lista2[i][j] == barco:
            total_bal2 += barco.municiones
            total_nav2 += 1
            barcos += 1

#Variables para poder llevar a cabo los porcentajes tanto de municiones como naves de el jugador uno y dos
X = total_bal * 0.70
Y = total_bal * 0.30

x = total_nav * 0.80
y = total_nav * 0.20

X1 = total_bal2 * 0.70
Y1 = total_bal2 * 0.30

x1 = total_nav2 * 0.80
y1 = total_nav2 * 0.20


# Para marcar donde haya disparado el jugador uno, haciendo cambios en la matriz del jugador 2
def marcar_pos(co, fi):
    if lista2[fi][co] == 0:
        print("\nEspacio vacio")
    elif lista2[fi][co] == "X":
        print("\nYa habia destruído una nave en esta posición")
    elif lista2[fi][co] == tanque:
        print("\nHa derrivado un Tanque!")
    elif lista2[fi][co] == helicop:
        print("\nHa derrivado un Helicoptero!")
    elif lista2[fi][co] == vortex:
        print("\nHa derrivado un Vortex!")
    elif lista2[fi][co] == barco:
        print("\nHa derrivado un Barco!")
        return False
    if turno:
        lista2[fi][co] = "X"
    else:
        pass
    return True


# Para marcar donde vaya a disparar el jugador 2 en la matriz del jugador uno
def marcar_pos2(co, fi):
    if lista[fi][co] == 0:
        print("\nEspacio vacio")
    elif lista[fi][co] == "X":
        print("\nYa habia destruído una nave en esta posición")
    elif lista[fi][co] == nave_rex:
        print("\nHa derrivado Nave Rex!")
    elif lista[fi][co] == nave_tuak:
        print("\nHa derrivado una Nave Tuak!")
    elif lista[fi][co] == nave_jouk:
        print("\nHa derrivado una Nave Jouk!")
    elif lista[fi][co] == nave_hunt:
        print("\nHa derrivado una nave Hunt!")
        return False
    if turno:
        lista[fi][co] = "X"
    else:
        lista[fi][co] = "X"
    return True


titulo = print("GUERRA DE LOS MUNDOS".center(35, '*'))

# Main principal, donde vamos a interactuar con el usuario
while True:
    menu = input("Desea comenzar? (s/n): ")
    op = menu.lower()
    if op == 'n':
        print("Gracias por jugar!")
        break
    elif op == 's':
        j1 = input("\nNombre del Jugador 1: ")
        j2 = input("Nombre del Jugador 2: ")
        ja = ""
        while True:
            print(lista)
            print(lista2)
            if turno:
                ja = j1
            else:
                ja = j2
            print("\nJugador actual: ", ja)
            print("\nPara columna -> 0 , 1 , 2 , 3\n"
                  "Para fila -> 0 , 1 , 2 , 3")
            co = int(input("Digite la posición de columna para atacar:  "))
            if co > 3 or co < 0:
                print("Numeros ingresados fuera de rango")
                break

            fi = int(input("Digite la posicion de fila: "))
            if fi > 3 or fi < 0:
                print("Numeros ingresasdos fuera de rango")
                break
            if marcar_pos(co, fi):
            #La funcion de hacer refrescar datos de las dos matrices
                tanques_1 = 0
                helicopteros_1 = 0
                barcos_1 = 0
                vortexs_1 = 0

                total_bal_2 = 0
                total_nav_2 = 0

                total_nav_1 = 0
                total_bal_1 = 0

                naves_rex_1 = 0
                naves_tuak_1 = 0
                naves_jouk_1 = 0
                naves_hunt_1 = 0
                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista2[i][j] == "X":
                            total_bal_1 = total_bal_1 - 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista2[i][j] == tanque:
                            total_bal_2 += tanque.municiones
                            total_nav_2 += 1
                            tanques_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista2[i][j] == vortex:
                            total_bal_2 += vortex.municiones
                            total_nav_2 += 1
                            vortexs_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista2[i][j] == helicop:
                            total_bal_2 += helicop.municiones
                            total_nav_2 += 1
                            helicopteros_1 += 1
                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista2[i][j] == barco:
                            total_bal_2 += barco.municiones
                            total_nav_2 += 1
                            barcos_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista[i][j] == "X":
                            total_bal_2 = total_bal_2 - 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista[i][j] == nave_rex:
                            total_bal_1 += nave_rex.municiones
                            total_nav_1 += 1
                            naves_rex_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista[i][j] == nave_tuak:
                            total_bal_1 += nave_tuak.municiones
                            total_nav_1 += 1
                            naves_tuak_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista[i][j] == nave_jouk:
                            total_bal_1 += nave_jouk.municiones
                            total_nav_1 += 1
                            naves_jouk_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista[i][j] == nave_hunt:
                            total_bal_1 += nave_hunt.municiones
                            total_nav_1 += 1
                            naves_hunt_1 += 1
                #Variables para saber el total de naves destruidas
                naves_derr2 = total_nav2 - total_nav_2
                naves_derr = total_nav - total_nav_1
                #Aquí ya se aplica condicionales para ver si ya cumplen los porcentajes
                if total_nav_2 >= x and total_nav_1 <= y:
                    print("\nHa ganado el jugador {}!\n".format(j2))
                    print("El jugador al tener mas del 80% de naves :", x1)
                    break
                if total_bal_2 >= X and total_bal_1 <= Y:
                    print("\nHa ganado el jugador {}! \n".format(j2))
                    print("El jugador al tener mas del 70% de municiones : ", x1)
                    break
                aban = input("Desea abandonar la partida? (s/n):")
                aban = aban.lower()
                if aban == 's':

                    print("\n{} ha ganado la partida!".format(j2))
                    print("\nJugador 1")
                    print("Cantidad de naves derrivadas", naves_derr)
                    print("Total de naves : ", total_nav)

                    print("\nJugador 2")
                    print("Cantidad de naves derrivadas", naves_derr2)
                    print("Total de naves : ", total_nav2)
                    break
                elif aban == 'n':
                    turno = not turno
                    print(lista)
                    print(lista2)
                    print("\nJugador actual: ", j2)
                    print("\nPara columna -> 0 , 1 , 2 , 3\n"
                          "Para fila -> 0 , 1 , 2 , 3")

                    co = int(input("Digite la posición de columna para atacar:  "))
                    if co > 3 or co < 0:
                        print("Numeros ingresados fuera de rango")
                        break

                    fi = int(input("Digite la posicion de fila: "))
                    if fi > 3 or fi < 0:
                        print("Numeros ingresasdos fuera de rango")
                        break
                    #Inicio de el turno de el segungo jugador
                    if marcar_pos2(co, fi):
                        total_bal_1 = 0
                        total_nav_1 = 0

                        naves_rex_1 = 0
                        naves_tuak_1 = 0
                        naves_jouk_1 = 0
                        naves_hunt_1 = 0

                        total_nav_2 = 0
                        total_bal_2 = 0

                        tanques_1 = 0
                        helicopteros_1 = 0
                        barcos_1 = 0
                        vortexs_1 = 0

                        for i in range(0, 4):
                            for j in range(0, 4):
                                if lista[i][j] == "X":
                                    total_bal_2 = total_bal_2 - 1

                        for i in range(0, 4):
                            for j in range(0, 4):
                                if lista[i][j] == nave_rex:
                                    total_bal_1 += nave_rex.municiones
                                    total_nav_1 += 1
                                    naves_rex_1 += 1

                        for i in range(0, 4):
                            for j in range(0, 4):
                                if lista[i][j] == nave_tuak:
                                    total_bal_1 += nave_tuak.municiones
                                    total_nav_1 += 1
                                    naves_tuak_1 += 1

                        for i in range(0, 4):
                            for j in range(0, 4):
                                if lista[i][j] == nave_jouk:
                                    total_bal_1 += nave_jouk.municiones
                                    total_nav_1 += 1
                                    naves_jouk_1 += 1

                        for i in range(0, 4):
                            for j in range(0, 4):
                                if lista[i][j] == nave_hunt:
                                    total_bal_1 += nave_hunt.municiones
                                    total_nav_1 += 1
                                    naves_hunt_1 += 1

                        for i in range(0, 4):
                            for j in range(0, 4):
                                if lista2[i][j] == "X":
                                    total_bal_1 = total_bal_1 - 1

                        for i in range(0, 4):
                            for j in range(0, 4):
                                if lista2[i][j] == tanque:
                                    total_bal_2 += tanque.municiones
                                    total_nav_2 += 1
                                    tanques_1 += 1

                        for i in range(0, 4):
                            for j in range(0, 4):
                                if lista2[i][j] == vortex:
                                    total_bal_2 += vortex.municiones
                                    total_nav_2 += 1
                                    vortexs_1 += 1

                        for i in range(0, 4):
                            for j in range(0, 4):
                                if lista2[i][j] == helicop:
                                    total_bal_2 += helicop.municiones
                                    total_nav_2 += 1
                                    helicopteros_1 += 1

                        for i in range(0, 4):
                            for j in range(0, 4):
                                if lista2[i][j] == barco:
                                    total_bal_2 += barco.municiones
                                    total_nav_2 += 1
                                    barcos_1 += 1
                        naves_derr2 = total_nav2 - total_nav_2
                        naves_derr = total_nav - total_nav_1
                        if total_nav_1 >= x1 and total_nav_2 <= y1:
                            print("\nHa ganado el jugador {}!\n".format(j1))
                            print("El jugador al tener mas del 80% de naves :", x1)
                            break
                        if total_bal_1 >= X1 and total_bal_2 <= Y1:
                            print("\nHa ganado el jugador {}! \n".format(j1))
                            print("El jugador al tener mas del 70% de municiones : ", x1)
                            break
                        aban = input("Desea abandonar la partida? (s/n):")
                        aban = aban.lower()
                        if aban == 's':
                            print("\n{} ha ganado la partida".format(j1))
                            print("\nJugador 1")
                            print("Cantidad de naves derrivadas", naves_derr)
                            print("Total de naves : ", total_nav)

                            print("\nJugador 2")
                            print("Cantidad de naves derrivadas", naves_derr2)
                            print("Total de naves : ", total_nav2)
                            break
                        elif aban == 'n':
                            pass
                        turno = not turno

            else:
                total_bal_1 = 0
                total_nav_1 = 0

                naves_rex_1 = 0
                naves_tuak_1 = 0
                naves_jouk_1 = 0
                naves_hunt_1 = 0

                total_nav_2 = 0
                total_bal_2 = 0

                tanques_1 = 0
                helicopteros_1 = 0
                barcos_1 = 0
                vortexs_1 = 0

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista[i][j] == "X":
                            total_bal_2 = total_bal_2 - 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista[i][j] == nave_rex:
                            total_bal_1 += nave_rex.municiones
                            total_nav_1 += 1
                            naves_rex_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista[i][j] == nave_tuak:
                            total_bal_1 += nave_tuak.municiones
                            total_nav_1 += 1
                            naves_tuak_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista[i][j] == nave_jouk:
                            total_bal_1 += nave_jouk.municiones
                            total_nav_1 += 1
                            naves_jouk_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista[i][j] == nave_hunt:
                            total_bal_1 += nave_hunt.municiones
                            total_nav_1 += 1
                            naves_hunt_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista2[i][j] == "X":
                            total_bal_1 = total_bal_1 - 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista2[i][j] == tanque:
                            total_bal_2 += tanque.municiones
                            total_nav_2 += 1
                            tanques_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista2[i][j] == vortex:
                            total_bal_2 += vortex.municiones
                            total_nav_2 += 1
                            vortexs_1 += 1

                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista2[i][j] == helicop:
                            total_bal_2 += helicop.municiones
                            total_nav_2 += 1
                            helicopteros_1 += 1
                for i in range(0, 4):
                    for j in range(0, 4):
                        if lista2[i][j] == barco:
                            total_bal_2 += barco.municiones
                            total_nav_2 += 1
                            barcos_1 += 1

                naves_derr2 = total_nav2 - total_nav_2
                naves_derr = total_nav - total_nav_1
                if total_nav_1 >= x1 and total_nav_2 <= y1:
                    print("\nHa ganado el jugador {}!\n".format(j1))
                    print("El jugador al tener mas del 80% de naves :", x1)
                    break
                if total_bal_1 >= X1 and total_bal_2 <= Y1:
                    print("\nHa ganado el jugador {}!\n".format(j1))
                    print("El jugador al tener mas del 70% de municiones : ", x1)
                    break
                aban = input("Desea abandonar la partida? (s/n):")
                aban = aban.lower()
                if aban == 's':
                    print("\n{} ha ganado la partida".format(j2))
                    print("\nJugador 1")
                    print("Total de naves derrivadas", naves_derr)
                    print("Total de naves : ", total_nav)

                    print("\nJugador 2")
                    print("Total de naves derrivadas", naves_derr2)
                    break
                elif aban == 'n':
                    pass
    else:
        print("Opción inválida, favor intente nuevamente")